const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const bcrypt = require('bcrypt');

const app = express();
app.use(express.json());
app.use(cors());

const PORT = process.env.PORT || 3000;

// Schema student
const studentSchema = mongoose.Schema({
    name: { type: String, required: true },
    prenoms: { type: String, required: true },
    matiere: { type: String, required: true },
    classe: { type: String, required: true }
});



// Student Model

const studentModel = mongoose.model('student',studentSchema)

// Schema Notes
const noteSchema = mongoose.Schema({
    student: { type: mongoose.Schema.Types.ObjectId, ref: 'student' },
    note: { type: Number, required: true }
});

// note Models
const noteModel = mongoose.model("note", noteSchema);

// User Schemma
const userSchema = mongoose.Schema({
    name:{type:String, required : true},
    prenoms:{type:String, required : true},
    email:{type:String, required : true , unique: true},
    age : {type : Number},
    password: { type: String, required: true }
})
// User Model 

const userModel = mongoose.model("users", userSchema)


app.get("/api/student",async(req,res)=>{
    try {
        const data = await studentModel.find({})
        res.json({success : true ,data : data })
    } catch (error) {
        res.json({message:"Erreur d'affichage d'etudiant"})
    }
})

app.get("/api/student/:id", async(req,res)=>{
    try {
        const id = req.params.id
        const data = await studentModel.findById(id)
        res.json({success : true , data : data})
    } catch (error) {
        res.json({message:"Erreur d'affichage de tous les etudiants"})
    }
})

app.post("/api/student/create", async (req, res) => {
    try {
        const data = new studentModel(req.body);
        await data.save();
        res.json({ success: true, message: "Etudiant ajouté avec succès", data: data });
    } catch (error) {
        console.error("Erreur d'ajout d'étudiant :", error);
        res.status(500).json({ message: "Erreur d'ajout d'étudiant" });
    }
});


app.delete("/api/student/delete/:id", async(req,res)=>{
    try {
        const id = req.params.id
        const data = await studentModel.deleteOne({_id:id})
        res.json({success : true , message :"Sppression d'etudiant reussis", data : data})
    } catch (error) {
        res.json({message:"Erreur d'ajout d'etudiant"})
    }
})

app.put("/api/student/update/:id", async(req,res)=>{
    try {
        const id = req.params.id
        const newData = req.body
        const data = await studentModel.findByIdAndUpdate(id,newData,{new:true})
        res.json({success : true , message :"Modification d'etudiant reussis", data : data})  
    } catch (error) {
        res.json({message:"Erreur de modifications d'etudiant"})
    }
})


//ETUDIANT ETUDIANT ETUDIANT
app.get("/api/users", async (req,res)=>{
    try {
        const data = await userModel.find({})
        res.json({success : true, data : data})
    } catch (error) {
        res.json({message:"Erreur d'affichage d'utilisateur'"})
    }
})

app.get("/api/user/:id", async (req,res)=>{
    try {
        const id = req.params.id
        const data = await userModel.findById(id)
        res.json({success : true , data : data})
    } catch (error) {
        res.json({message:"Erreur d'affichage d'utilisateur ID"})
    }
})


app.post("/api/user/create", async (req, res) => {
    try {
        const data = new userModel(req.body);
        await data.save();
        res.json({ success: true, message: "Ajout d'utilisateur réussi", data: data });
    } catch (error) {
        console.error("Erreur lors de l'ajout d'utilisateur :", error);
        res.status(500).json({ success: false, message: "Erreur lors de l'ajout d'utilisateur" });
    }
});


app.delete("/api/user/delete/:id",async(req,res)=>{
    try {
        const id = req.params.id
        const data = await userModel.findByIdAndDelete({_id:id})
        res.json({success : true , message :"Suppression d'utilisateur reussis", data : data})
    } catch (error) {
        res.json({message:"Erreur de suppression d'utilisateur"})
    }
})

app.put("/api/user/update/:id", async (req,res)=>{
    try {
        const id = req.params.id
        const newUser = req.body
        const data = await userModel.findByIdAndUpdate(id,newUser, {new : true})
        res.json({success : true , message : "Modification user reussis", data : data})
    } catch (error) {
        res.json({message:"Erreur lors de la modification d'utilisateur"})
    }
})
//NOTE NOTE NOTES



// Get all notes
app.get('/api/notes', async (req, res) => {
    try {
        const notes = await noteModel.find({}).populate('student', 'name');
        res.json({ success: true, data: notes });
    } catch (error) {
        console.error('Erreur lors de la récupération des notes :', error);
        res.status(500).json({ success: false, message: "Erreur lors de la récupération des notes." });
    }
});

// Get a single note by ID
app.get("/api/note/:id", async (req, res) => {
    try {
        const noteId = req.params.id;
        const data = await noteModel.findById(noteId).populate('student', 'name');
        res.json({ success: true, data: data });
    } catch (error) {
        res.json({ message: "Erreur lors de l'affichage de la note" });
    }
});

// Create a new note
app.post("/api/note/create", async (req, res) => {
    try {
        const { studentId, note } = req.body;

        // Create a new note with student ID and note value
        const newNote = new noteModel({ student: studentId, note });
        const savedNote = await newNote.save();

        res.json({ success: true, message: "Note ajoutée avec succès", data: savedNote });
    } catch (error) {
        console.error("Erreur lors de l'ajout de la note :", error);
        res.status(500).json({ success: false, message: "Erreur lors de l'ajout de la note." });
    }
});

// Update a note
app.put("/api/note/update/:id", async (req, res) => {
    try {
        const noteId = req.params.id;
        const { note } = req.body;

        const updatedNote = await noteModel.findByIdAndUpdate(noteId, { note }, { new: true });

        if (!updatedNote) {
            return res.status(404).json({ success: false, message: "Note non trouvée." });
        }

        res.json({ success: true, message: "Note mise à jour avec succès", data: updatedNote });
    } catch (error) {
        console.error("Erreur lors de la mise à jour de la note :", error);
        res.status(500).json({ success: false, message: "Erreur lors de la mise à jour de la note." });
    }
});

// Delete a note
app.delete("/api/note/delete/:id", async (req, res) => {
    try {
        const noteId = req.params.id;
        const deletedNote = await noteModel.findByIdAndDelete(noteId);

        if (!deletedNote) {
            return res.status(404).json({ success: false, message: "Note non trouvée." });
        }

        res.json({ success: true, message: "Note supprimée avec succès", data: deletedNote });
    } catch (error) {
        console.error("Erreur lors de la suppression de la note :", error);
        res.status(500).json({ success: false, message: "Erreur lors de la suppression de la note." });
    }
});

//MATIERE MATIERE MATIERE



// Route pour récupérer les matières disponibles
app.get("/api/matieres", async (req, res) => {
    try {
        // Récupérer la liste des matières à partir de votre base de données ou d'une source de données appropriée
        const matieres = await studentModel.distinct("matiere");
        res.json({ success: true, data: matieres });
    } catch (error) {
        console.error("Erreur lors de la récupération des matières :", error);
        res.status(500).json({ message: "Erreur lors de la récupération des matières." });
    }
});

// Affichage de chaque matière avec les noms des étudiants dans cette matière
app.get("/api/students/matiere/:matiere", async (req, res) => {
    try {
        const matiere = req.params.matiere;
        const students = await studentModel.find({ matiere });
        res.json({ success: true, students });
    } catch (error) {
        console.error("Erreur lors de l'affichage des étudiants par matière :", error);
        res.status(500).json({ message: "Erreur lors de l'affichage des étudiants par matière." });
    }
});


//CLASSE CLASSE CLASSE



// Route pour récupérer les Classes disponibles
app.get("/api/classe", async (req, res) => {
    try {
        // Récupérer la liste des matières à partir de votre base de données ou d'une source de données appropriée
        const classes = await studentModel.distinct("classe");
        res.json({ success: true, data: classes });
    } catch (error) {
        console.error("Erreur lors de la récupération des matières :", error);
        res.status(500).json({ message: "Erreur lors de la récupération des matières." });
    }
});

// Affichage de la liste des étudiants appartenant à la même classe
app.get("/api/students/classe/:classe", async (req, res) => {
    try {
        const classe = req.params.classe;
        const students = await studentModel.find({ classe });
        res.json({ success: true, students });
    } catch (error) {
        console.error("Erreur lors de l'affichage des étudiants par classe :", error);
        res.status(500).json({ message: "Erreur lors de l'affichage des étudiants par classe." });
    }
});






app.post('/api/login', async (req, res) => {
    const { email, password } = req.body;
    try {
        // Vérifiez si l'utilisateur existe dans la base de données
        const user = await userModel.findOne({ email });
        if (!user) {
            return res.status(401).json({ success: false, message: 'Invalid credentials' });
        }
        
        // Vérifiez si le mot de passe est correct
        const isPasswordValid = (password === user.password);
        if (!isPasswordValid) {
            return res.status(401).json({ success: false, message: 'Invalid credentials' });
        }
        
        // Si l'utilisateur existe et le mot de passe est correct, renvoyez une réponse réussie
        res.json({ success: true, message: 'Connexion réussie' });
    } catch (error) {
        console.error('Error logging in:', error);
        res.status(500).json({ success: false, message: "Échec lors de la connexion, veuillez réessayer plus tard" });
    }
});






mongoose.connect("mongodb://localhost:27017/gestion_note")
    .then(() => {
        app.listen(PORT, () => console.log("Port connecté", PORT))
    })
    .catch((err) => console.log(err));



    