/* eslint-disable react/prop-types */
import { Typography, Form, Input, Button, Modal, Table,message  } from 'antd';
import { useState, useEffect } from 'react';
import axios from 'axios';

axios.defaults.baseURL = 'http://localhost:3000/';

export default function Inventory() {
  return (
    <div>
      <Typography.Title level={3}>Etudiants</Typography.Title>
      <NoteForm />
    </div>
  );
}

function NoteForm() {
  const [selectedUser, setSelectedUser] = useState(null);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [formData, setFormData] = useState({
    name: '',
    prenoms: '',
    matiere: '',
    classe: ''
  });
  const [students, setStudents] = useState([]);

  useEffect(() => {
    fetchStudents();
  }, []);

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const fetchStudents = async () => {
    try {
      const response = await axios.get('/api/student');
      setStudents(response.data.data);
    } catch (error) {
      console.error('Erreur lors de la récupération des étudiants:', error);
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await axios.post('/api/student/create', { 
        name: formData.name,
        prenoms: formData.prenoms,
        matiere: formData.matiere,
        classe: formData.classe
      });
      console.log(response.data); 
      message.success(response.data.message);
      setIsModalOpen(false); 
      // Après l'ajout d'un nouvel étudiant, on rafraîchit la liste des étudiants
      fetchStudents();
      handleClose();
    } catch (error) {
      console.error('Erreur lors de l\'ajout de l\'étudiant:', error);
    }
  };

  const handleUpdate = async() =>{
    try {
      const response = await axios.put(`/api/student/update/${selectedUser._id}`,formData)
      console.log(response.data)
      message.success(response.data.message);
      fetchStudents()
      handleClose();
    } catch (error) {
      console.error('Erreur lors de la modification de l\'étudiant:', error);
    }
  }

  const handleDelete = async(id) =>{
    try {
      const response = await axios.delete(`/api/student/delete/${id}`)
      console.log(response.data)
      message.success(response.data.message);
      fetchStudents();
    } catch (error) {
      console.error('Erreur lors de la suppression de l\'étudiant:', error);
    }
  }
  
  const handleClose = () =>{
    setIsModalOpen(false);
    setSelectedUser(null);
    setFormData({
      name: '',
      prenoms: '',
      matiere: '',
      classe: ''
    });
  }



  const handleEdit = (student) => {
    setSelectedUser(student);
    setFormData(student);
    setIsModalOpen(true);
  };
  
  return (
    <>
      <Button type="primary" onClick={showModal}>
        Open Modal
      </Button>
      <Modal title="Add Note" visible={isModalOpen} footer={null} onCancel={() => setIsModalOpen(false)}>
        <Form
          labelCol={{
            span: 6,
          }}
          wrapperCol={{
            span: 18,
          }}
          name="note"
          initialValues={{
            remember: true,
          }}
        >
          <Form.Item
            label="Nom"
            name="name"
            rules={[
              {
                required: true,
                message: 'Entrez votre nom',
              },
            ]}
          >
            <Input name="name" value={formData.name} onChange={handleInputChange} />
          </Form.Item>

          <Form.Item
            label="Prénoms"
            name="prenoms"
            rules={[
              {
                required: true,
                message: 'Entrez vos prénoms',
              },
            ]}
          >
            <Input name="prenoms" value={formData.prenoms} onChange={handleInputChange} />
          </Form.Item>

          <Form.Item
            label="Matière"
            name="matiere"
            rules={[
              {
                required: true,
                message: 'Entrez votre matière',
              },
            ]}
          >
            <Input name="matiere" value={formData.matiere} onChange={handleInputChange} />
          </Form.Item>

          <Form.Item
            label="Classe"
            name="classe"
            rules={[
              {
                required: true,
                message: 'Entrez votre classe',
              },
            ]}
          >
            <Input name="classe" value={formData.classe} onChange={handleInputChange} />
          </Form.Item>

          <Form.Item
            wrapperCol={{
              offset: 6,
              span: 18,
            }}
          >
            <Button type="primary" htmlType="submit" onClick={selectedUser ? handleUpdate : handleSubmit}>
              {selectedUser ? 'Update' : 'Submit'}
            </Button>
          </Form.Item>
        </Form>
      </Modal>
      <StudentList students={students} handleEdit={handleEdit} handleDelete={handleDelete} />
    </>
  );
}

function StudentList({ students, handleEdit, handleDelete }) {
  const columns = [
    {
      title: 'Nom',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Prénoms',
      dataIndex: 'prenoms',
      key: 'prenoms',
    },
    {
      title: 'Matière',
      dataIndex: 'matiere',
      key: 'matiere',
    },
    {
      title: 'Classe',
      dataIndex: 'classe',
      key: 'classe',
    },
    {
      title: 'Actions',
      key: 'actions',
      render: (text, record) => (
        <span>
          <Button type="link" onClick={() => handleEdit(record)}>Edit</Button>
          <Button type="link" danger onClick={() => handleDelete(record._id)}>Delete</Button>
        </span>
      ),
    },
  ];

  return (
    <div>
      <Typography.Title level={4}>Liste des étudiants</Typography.Title>
      <Table columns={columns} dataSource={students}/>
    </div>
  );
}

