import { Typography, Table } from 'antd';
import { useState, useEffect } from 'react';
import axios from 'axios';

export default function Orders() {
    axios.defaults.baseURL = 'http://localhost:3000/';

    return (
        <div>
            <Typography.Title level={3}>Classe</Typography.Title>
            <StudentListByClasse />
        </div>
    );
}

function StudentListByClasse() {
    const [classes, setClasses] = useState([]);

    useEffect(() => {
        fetchClasses();
    }, []);

    const fetchClasses = async () => {
        try {
            const response = await axios.get('/api/classe');
            setClasses(response.data.data);
        } catch (error) {
            console.error('Erreur lors de la récupération des classes :', error);
        }
    };

    return (
        <div>
            {classes.map((classe, index) => (
                <div key={index}>
                    <Typography.Title level={4}>{classe}</Typography.Title>
                    <StudentList classe={classe} />
                </div>
            ))}
        </div>
    );
}

// eslint-disable-next-line react/prop-types
function StudentList({ classe }) {
    const [students, setStudents] = useState([]);

    useEffect(() => {
        fetchStudentsByClasse();
    }, []);

    const fetchStudentsByClasse = async () => {
        try {
            const response = await axios.get(`/api/students/classe/${classe}`);
            setStudents(response.data.students);
        } catch (error) {
            console.error(`Erreur lors de la récupération des étudiants pour la classe ${classe} :`, error);
        }
    };

    const columns = [
        {
            title: 'Nom',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: 'Prénoms',
            dataIndex: 'prenoms',
            key: 'prenoms',
        },
    ];

    return (
        <Table columns={columns} dataSource={students} rowKey="_id" />
    );
}
