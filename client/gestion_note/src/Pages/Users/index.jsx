import { Typography, Button, Form, Input, Table, Modal,InputNumber,message } from 'antd';
import { useEffect, useState } from 'react';
import axios from 'axios';

axios.defaults.baseURL = 'http://localhost:3000/';

export default function Users() {
  return (
    <div>
      <Typography.Title>Liste des Utilisateurs</Typography.Title>
      <UserForm />
    </div>
  );
}

function UserForm() {
  const [users, setUsers] = useState([]);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [selectUser, setSelectUser] = useState(null);
  const [formData, setFormData] = useState({
    name: '',
    prenoms: '',
    email: '',
    age: '',
    password: ''
  });

  useEffect(() => {
    fetchUser();
  }, []);

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const fetchUser = async () => {
    try {
      const response = await axios.get("/api/users");
      setUsers(response.data.data);
    } catch (error) {
      console.error('Erreur lors de la récupération des Utilisateurs:', error);
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await axios.post("/api/user/create", formData);
      console.log(response.data);
      message.success(response.data.message);
      setIsModalOpen(false);
      fetchUser();
      handleClose();
    } catch (error) {
      console.error('Erreur lors de l\'ajout de l\'utilisateur:', error);
    }
  };

  const handleUpdate = async () => {
    try {
      const response = await axios.put(`/api/user/update/${selectUser._id}`, formData);
      console.log(response.data);
      message.success(response.data.message);
      fetchUser();
      handleClose();
    } catch (error) {
      console.error('Erreur lors de l\'ajout de l\'étudiant:', error);
    }
  };

  const handleDelete = async (id) => {
    try {
      const response = await axios.delete(`/api/user/delete/${id}`);
      console.log(response.data);
      message.success(response.data.message);
      fetchUser();
    } catch (error) {
      console.error('Erreur lors de la suppression de l\'utilisateur:', error);
    }
  };

  const handleClose = () => {
    setIsModalOpen(false);
    setSelectUser(null);
    setFormData({
      name: '',
      prenoms: '',
      age: '',
      email: '',
      password:''
    });
  };

  const handleEdit = (user) => {
    setSelectUser(user);
    setFormData(user);
    setIsModalOpen(true);
  };

  return (
    <>
      <Button type="primary" onClick={showModal}>
        Ajouter un utilisateur
      </Button>
      <Modal
        title="Ajouter un utilisateur"
        footer={null} visible={isModalOpen} onCancel={() => setIsModalOpen(false)}
      >
        <Form
          labelCol={{
            span: 6,
          }}
          wrapperCol={{
            span: 18,
          }}
          name="user"
          initialValues={{
            remember: true,
          }}
        >
          <Form.Item
            label="Nom"
            name="name"
            rules={[
              {
                required: true,
                message: 'Entrez votre nom',
              },
            ]}
          >
            <Input name="name" value={formData.name} onChange={handleChange} />
          </Form.Item>

          <Form.Item
            label="Prénoms"
            name="prenoms"
            rules={[
              {
                required: true,
                message: 'Entrez vos prénoms',
              },
            ]}
          >
            <Input name="prenoms" value={formData.prenoms} onChange={handleChange} />
          </Form.Item>

          <Form.Item
            label="Email"
            name="email"
            rules={[
              {
                required: true,
                message: 'Entrez votre email',
              },
            ]}
          >
            <Input name="email" value={formData.email} onChange={handleChange} />
          </Form.Item>

          <Form.Item
  label="Mot de passe"
  name="password"
  rules={[
    {
      required: true,
      message: 'Entrez votre mot de passe',
    },
  ]}
>
  <Input.Password
    name="password"
    value={formData.password}
    onChange={handleChange}
  />
</Form.Item>


          <Form.Item
  label="Age"
  name="age"
  rules={[
    {
      required: true,
      message: 'Veuillez saisir votre âge.',
    },
    {
      type: 'number',
    },
  ]}
>
  <InputNumber
    name="age"
    value={formData.age}
    onChange={(value) => handleChange({ target: { name: 'age', value } })} // Passer l'événement avec le nom et la valeur
    style={{ width: '100%' }} // Ajouter des styles pour définir la largeur du champ de saisie
  />
</Form.Item>


          <Form.Item
            wrapperCol={{
              offset: 6,
              span: 18,
            }}
          >
            <Button type="primary" htmlType="submit" onClick={selectUser ? handleUpdate : handleSubmit}>
              {selectUser ? 'Update' : 'Submit'}
            </Button>
          </Form.Item>
        </Form>
      </Modal>
      <UserList users={users} handleEdit={handleEdit} handleDelete={handleDelete} />
    </>
  );
}

// eslint-disable-next-line react/prop-types
function UserList({ users, handleEdit, handleDelete }) {
  const columns = [
    {
      title: 'Nom',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Prénoms',
      dataIndex: 'prenoms',
      key: 'prenoms',
    },
    {
      title: 'Email',
      dataIndex: 'email',
      key: 'email',
    },
    {
      title: 'Âge',
      dataIndex: 'age', // Assurez-vous que 'age' correspond à la clé utilisée pour l'âge dans les données utilisateur
      key: 'age',
    },
    {
      title: 'Password',
      dataIndex: 'password', // Assurez-vous que 'age' correspond à la clé utilisée pour l'âge dans les données utilisateur
      key: 'password',
    },
    {
      title: 'Action',
      key: 'action',
      render: (text, record) => (
        <span>
          <Button type='link' onClick={() => handleEdit(record)}>Edit</Button>
          <Button type='link' danger onClick={() => handleDelete(record._id)}>Delete</Button>
        </span>
      )
    },
  ];

  return (
    <div>
      <Typography.Title level={4}>Liste des utilisateurs</Typography.Title>
      <Table columns={columns} dataSource={users} />
    </div>
  );
}
