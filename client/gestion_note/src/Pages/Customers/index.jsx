import { Typography, Table } from 'antd';
import { useState, useEffect } from 'react';
import axios from 'axios';

export default function Inventory() {
    axios.defaults.baseURL = 'http://localhost:3000/';

    return (
        <div>
            <Typography.Title level={3}>Matières</Typography.Title>
            <StudentListByMatiere />
        </div>
    );
}

function StudentListByMatiere() {
    const [matieres, setMatieres] = useState([]);

    useEffect(() => {
        fetchMatieres();
    }, []);

    const fetchMatieres = async () => {
        try {
            const response = await axios.get('/api/matieres');
            setMatieres(response.data.data);
        } catch (error) {
            console.error('Erreur lors de la récupération des matières :', error);
        }
    };

    return (
        <div>
            {matieres.map((matiere, index) => (
                <div key={index}>
                    <Typography.Title level={4}>{matiere}</Typography.Title>
                    <StudentList matiere={matiere} />
                </div>
            ))}
        </div>
    );
}

// eslint-disable-next-line react/prop-types
function StudentList({ matiere }) {
    const [students, setStudents] = useState([]);

    useEffect(() => {
        fetchStudentsByMatiere();
    }, []);

    const fetchStudentsByMatiere = async () => {
        try {
            const response = await axios.get(`/api/students/matiere/${matiere}`);
            setStudents(response.data.students);
        } catch (error) {
            console.error(`Erreur lors de la récupération des étudiants pour la matière ${matiere} :`, error);
        }
    };

    const columns = [
        {
            title: 'Nom',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: 'Prénoms',
            dataIndex: 'prenoms',
            key: 'prenoms',
        },
    ];

    return (
        <Table columns={columns} dataSource={students} rowKey="_id" />
    );
}
