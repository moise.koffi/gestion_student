import { Typography, Select, Form, Input, Button, message, Table, Space, Modal } from 'antd';
import { useState, useEffect } from 'react';
import axios from 'axios';

export default function Dashboard() {
    axios.defaults.baseURL = 'http://localhost:3000/';

    // Créer un état pour stocker les notes
    const [notes, setNotes] = useState([]);
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [editingNote, setEditingNote] = useState(null);

    useEffect(() => {
        fetchNotes();
    }, []);

    const fetchNotes = async () => {
        try {
            const response = await axios.get('/api/notes');
            setNotes(response.data.data);
        } catch (error) {
            console.error('Erreur lors de la récupération des notes :', error);
        }
    };

    // Fonction pour actualiser les notes après l'enregistrement d'une nouvelle note ou la modification ou la suppression
    const handleNoteChange = () => {
        fetchNotes();
    };

    const handleEdit = (record) => {
        setEditingNote(record);
        setIsModalVisible(true);
    };

    const handleDelete = async (record) => {
        try {
            const response = await axios.delete(`/api/note/delete/${record._id}`);
            if (response.data.success) {
                message.success(response.data.message);
                handleNoteChange();
            } else {
                message.error(response.data.message);
            }
        } catch (error) {
            console.error('Erreur lors de la suppression de la note :', error);
            message.error('Erreur lors de la suppression de la note. Veuillez réessayer.');
        }
    };

    return (
        <div>
            <Typography.Title level={2}>Notes</Typography.Title>
            <Typography.Title level={3}>Notes par Matière</Typography.Title>
            <AddNoteForm onNoteAdded={handleNoteChange} />
            {/* Passer les données des notes en tant que prop */}
            <NotesTable notes={notes} onEdit={handleEdit} onDelete={handleDelete} />
            <EditNoteModal note={editingNote} isVisible={isModalVisible} onEdit={handleNoteChange} onClose={() => setIsModalVisible(false)} />
        </div>
    );
}

// eslint-disable-next-line react/prop-types
function AddNoteForm({ onNoteAdded }) {
    const [formData, setFormData] = useState({
        studentId: '',
        note: '',
    });

    const [students, setStudents] = useState([]);

    useEffect(() => {
        fetchStudents();
    }, []);

    const fetchStudents = async () => {
        try {
            const response = await axios.get('/api/student');
            setStudents(response.data.data);
        } catch (error) {
            console.error('Erreur lors de la récupération des étudiants :', error);
        }
    };

    const handleInputChange = (name, value) => {
        setFormData({ ...formData, [name]: value });
    };

    const handleSubmit = async () => {
        try {
            const response = await axios.post('/api/note/create', {
                studentId: formData.studentId,
                note: formData.note
            });
            if (response.data.success) {
                message.success(response.data.message);
                setFormData({  // Réinitialiser le formulaire
                    studentId: '',
                    note: '',
                });
                // Appeler la fonction de rappel pour actualiser les notes
                onNoteAdded();
            } else {
                message.error(response.data.message);
            }
        } catch (error) {
            console.error('Erreur lors de l\'ajout de la note :', error);
            message.error('Erreur lors de l\'ajout de la note. Veuillez réessayer.');
        }
    };

    return (
        <Form>
            <Form.Item label="Étudiant" name="studentId" rules={[{ required: true, message: 'Veuillez sélectionner un étudiant.' }]}>
                <Select onChange={(value) => handleInputChange('studentId', value)} value={formData.studentId}>
                    {students.map((student) => (
                        <Select.Option key={student._id} value={student._id}>
                            {student.name} {student.prenoms}
                        </Select.Option>
                    ))}
                </Select>
            </Form.Item>
            <Form.Item label="Note" name="note" rules={[{ required: true, message: 'Veuillez saisir la note.' }]}>
                <Input type="number" onChange={(e) => handleInputChange('note', e.target.value)} value={formData.note} />
            </Form.Item>
            <Form.Item>
                <Button type="primary" onClick={handleSubmit}>Ajouter la note</Button>
            </Form.Item>
        </Form>
    );
}

// eslint-disable-next-line react/prop-types
function NotesTable({ notes, onEdit, onDelete }) {
    const columns = [
        {
            title: 'Étudiant',
            dataIndex: 'student',
            key: 'student',
            render: student => (
                <span>{student && student.name} {student && student.prenoms}</span>
            ),
        },
        {
            title: 'Note',
            dataIndex: 'note',
            key: 'note',
        },
        {
            title: 'Action',
            key: 'action',
            render: (text, record) => (
                <Space size="middle">
                    <Button onClick={() => onEdit(record)}>Modifier</Button>
                    <Button onClick={() => onDelete(record)}>Supprimer</Button>
                </Space>
            ),
        },
    ];

    return (
        <div>
            <Typography.Title level={3}>Liste des Notes</Typography.Title>
            <Table dataSource={notes} columns={columns} />
        </div>
    );
}

// eslint-disable-next-line react/prop-types
function EditNoteModal({ note, isVisible, onEdit, onClose }) {
    const [editedNote, setEditedNote] = useState(note);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        setEditedNote(note);
    }, [note]);

    const handleInputChange = (name, value) => {
        setEditedNote({ ...editedNote, [name]: value });
    };

    const handleSave = async () => {
        try {
            setLoading(true);
            const response = await axios.put(`/api/note/update/${editedNote._id}`, editedNote);
            if (response.data.success) {
                message.success(response.data.message);
                onEdit();
                onClose();
            } else {
                message.error(response.data.message);
            }
        } catch (error) {
            console.error('Erreur lors de la modification de la note :', error);
            message.error('Erreur lors de la modification de la note. Veuillez réessayer.');
        } finally {
            setLoading(false);
        }
    };

    return (
        <Modal
            title="Modifier la note"
            visible={isVisible}
            onOk={handleSave}
            onCancel={onClose}
            confirmLoading={loading}
        >
            <Form>
                <Form.Item label="Note" name="note">
                    <Input type="number" value={editedNote ? editedNote.note : ''} onChange ={(e) => handleInputChange('note', e.target.value)} />
                </Form.Item>
            </Form>
        </Modal>
    );
}

