import {Image ,Space,Badge, Typography} from 'antd'
import { BellFilled, MailOutlined } from '@ant-design/icons';
export default function AppHeader() {
  return (
    <div className="AppHeaders">
      <Image 
      width={55}
      src='https://th.bing.com/th/id/OIP.kpAg6_oxwb3kfiYKDeJlVwHaHa?rs=1&pid=ImgDetMain' > 
      </Image>
      <Typography.Title>Pannel d'administration</Typography.Title>

    <Space>
        <Badge count ={10} dot>
            <MailOutlined style ={{fontSize: 24}} />
        </Badge>
        <Badge count ={20} dot>
             <BellFilled style ={{fontSize: 24}}/>
        </Badge>
            
    </Space>
    </div>
  )
}
