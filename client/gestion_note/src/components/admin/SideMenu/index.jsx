
import { Menu } from 'antd';
import {useNavigate} from 'react-router-dom' 
import { AppstoreOutlined, ShopOutlined, ShoppingCartOutlined, UserOutlined } from '@ant-design/icons';
// import { Link } from "react-router-dom";


export default function SideMenu() {
    const navigate = useNavigate()
  return (
    <div className="SideMenu">
      <Menu mode="vertical" 
        
        onClick={(items)=>{navigate(items.key)}}
      
      items={[
        {
          key: '/dashboard',
          icon: <AppstoreOutlined />,
          label: "Notes",
          
        },
        {
          key: '/inventory',
          icon: <ShopOutlined />,
          label: 'Etudiants',
          
        },
        {
          key: '/orders',
          icon: <ShoppingCartOutlined />,
          label: 'Classes',
          
        },
        {
          key: '/customers',
          icon: <UserOutlined />,
          label: 'Matières',
          
        },
        {
          key: '/users',
          icon: <UserOutlined />,
          label: 'Utilisateurs',
          
        }
      ]}/>
    </div>
  );
}
