import { Routes, Route } from 'react-router-dom';
import Dashboard from '../../../Pages/Dashboard';
import Inventory from '../../../Pages/Inventory';
import Orders from '../../../Pages/Orders';
import Customers from '../../../Pages/Customers';
import Users from '../../../Pages/Users';

export default function AppRoutes() {
  return (
    <Routes>
      <Route path="/dashboard" element={<Dashboard />} />
      <Route path="/inventory" element={<Inventory />} />
      <Route path="/orders" element={<Orders />} />
      <Route path="/customers" element={<Customers />} />
      <Route path="/users" element={<Users />} />
    </Routes>
  );
}
