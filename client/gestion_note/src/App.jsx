// App.jsx
import { BrowserRouter as Router } from 'react-router-dom';
import { Space } from 'antd';
import AppHeader from './components/admin/AppHeader';
import AppFooter from './components/admin/AppFooter';
import SideMenu from './components/admin/SideMenu'; // Assurez-vous d'importer SideMenu
import PageContent from "./components/admin/PageContent";

const App = () => {
  return (
    <Router>
      <div className='App'>
        <AppHeader />
          <Space>
            <SideMenu /> {/* Assurez-vous que SideMenu est à l'intérieur de Router */}
            <PageContent/>
          </Space>
        <AppFooter />
      </div>
    </Router>
  );
};

export default App;
